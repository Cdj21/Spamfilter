# SPAMFILTER ENRON DATA MET NAIVE BAYES CLASSIFIER

# ALLE IMPORTS

import os
import string
import time

from nltk.corpus import stopwords
from nltk.classify import NaiveBayesClassifier
import nltk.classify.util
from nltk.tokenize import word_tokenize
import random

start_time = time.time()

rootdir = "/Users/Caspar/Desktop/Enron data"
# rootdir2 = "/Users/Caspar/Desktop/Enron data/enron1/ham"

ham_list = []

spam_list = []

for directory, subdirectory, filenames in os.walk(rootdir):
    if os.path.split(directory)[1] == 'ham':
        print(directory, subdirectory, len(filenames))
        for file in filenames:
            with open(os.path.join(directory, file), encoding="latin-1") as f:
                data = f.read()

                tekst = data.lower()
                lijst_met_woorden = word_tokenize(tekst)

                currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
                for c in lijst_met_woorden:
                    b = lijst_met_woorden.index(c)
                    if c in currencies:
                        lijst_met_woorden[b] = "geldteken"

                leesteken = string.punctuation
                lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

                # woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
                # print(woordsoort_functie)

                stopw = set(stopwords.words())
                lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in stopw]

                for po in lijst_met_woorden:
                    a = lijst_met_woorden.index(po)
                    if po.isdigit():
                        lijst_met_woorden[a] = "getal"

                def naivebayes_woordenboek(lijst_met_woorden):
                    mijn_woordenboek = dict([(word, True) for word in lijst_met_woorden])
                    return mijn_woordenboek

                ham_list.append((naivebayes_woordenboek(lijst_met_woorden), "ham"))

    if os.path.split(directory)[1] == 'spam':
        print(directory, subdirectory, len(filenames))
        for file in filenames:
            with open(os.path.join(directory, file), encoding="latin-1") as f:
                data = f.read()

                tekst = data.lower()
                lijst_met_woorden = word_tokenize(tekst)

                currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
                for c in lijst_met_woorden:
                    b = lijst_met_woorden.index(c)
                    if c in currencies:
                        lijst_met_woorden[b] = "geldteken"

                leesteken = string.punctuation
                lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

                # woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
                # print(woordsoort_functie)

                stopw = set(stopwords.words())
                lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in stopw]

                for po in lijst_met_woorden:
                    a = lijst_met_woorden.index(po)
                    if po.isdigit():
                        lijst_met_woorden[a] = "getal"

                def naivebayes_woordenboek(lijst_met_woorden):
                    mijn_woordenboek = dict([(word, True) for word in lijst_met_woorden])
                    return mijn_woordenboek

                spam_list.append((naivebayes_woordenboek(lijst_met_woorden), "spam"))

combined_list = ham_list + spam_list
random.shuffle(combined_list)

training_part = int(len(combined_list) * .7)

training_set = combined_list[:training_part]
test_set = combined_list[training_part:]

classifier = NaiveBayesClassifier.train(training_set)
accuracy = nltk.classify.accuracy(classifier, test_set)
frequency = nltk.FreqDist(combined_list)

file = "/Users/Caspar/Desktop/test_mails/d.txt"

with open(file, "r") as f:
    file_data = f.read()


def mail_analyse(file_data):

    file_data = file_data.lower()

    lijst_met_woorden = word_tokenize(file_data)

    currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
    for element1 in lijst_met_woorden:
        index_element1 = lijst_met_woorden.index(element1)
        if element1 in currencies:
            lijst_met_woorden[index_element1] = "geldteken"

    for element2 in lijst_met_woorden:
        index_element2 = lijst_met_woorden.index(element2)
        if element2.isdigit():
            lijst_met_woorden[index_element2] = "getal"

    leesteken = string.punctuation
    lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

    woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
    tekens = []
    for element4 in lijst_met_woorden:
        for ws_functie_element4 in woordsoort_functie:
            if ws_functie_element4 == (element4, 'POS') or ws_functie_element4 == (element4, ':')\
                    or ws_functie_element4 == (element4, '``'):
                tekens.append(element4)

    lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in tekens]

    for element5 in lijst_met_woorden:
        index_element5 = lijst_met_woorden.index(element5)
        for ws_functie_element5 in woordsoort_functie:
            if ws_functie_element5 == (element5, 'CD'):
                lijst_met_woorden[index_element5] = "getal"

    stopw = set(stopwords.words())
    lijst_met_woorden = [element6 for element6 in lijst_met_woorden if element6 not in stopw]

    lijst_met_woorden = {element7: True for element7 in lijst_met_woorden}

    return lijst_met_woorden


gefilterde_mail = mail_analyse(file_data)


print("Eerste ham email" "\n")
print(ham_list[0])
print("Aantal emails in ham_list")
print(len(ham_list))

print("\n" "Eerste spam email" "\n")
print(spam_list[0])
print("Aantal emails in spam_list")
print(len(spam_list))


print(len(training_set))
print(len(test_set))

print("Accuracy = ", accuracy * 100)
classifier.show_most_informative_features(20)
print(frequency.most_common(50))

print("Mail is :", classifier.classify(gefilterde_mail))

print("--- %s seconds ---" % (time.time() - start_time))
