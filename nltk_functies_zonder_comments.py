# FUNCTIES VOOR TEXT FILTERING

# ALLE IMPORTS
import string
import nltk.classify.util
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


tekst = " get your him free , @ < . /?#, adware scan and removal download 6$ gbp now. http : / / zk"


tekst = tekst.lower()
lijst_met_woorden = word_tokenize(tekst)

currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
for c in lijst_met_woorden:
    b = lijst_met_woorden.index(c)
    if c in currencies:
        lijst_met_woorden[b] = "geldteken"

leesteken = string.punctuation
lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
print(woordsoort_functie)

stopw = set(stopwords.words())
lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in stopw]

for po in lijst_met_woorden:
    a = lijst_met_woorden.index(po)
    if po.isdigit():
        lijst_met_woorden[a] = "getal"

def NaiveBayes_woordenboek(lijst_met_woorden):
    mijn_woordenboek = dict([(word, True) for word in lijst_met_woorden])
    return mijn_woordenboek

print(NaiveBayes_woordenboek(lijst_met_woorden))
