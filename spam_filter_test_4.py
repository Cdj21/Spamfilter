# SPAMFILTER ENRON DATA MET NAIVE BAYES CLASSIFIER

# ALLE IMPORTS

import string
import time
import nltk.classify.util
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

start_time = time.time()
file = "/Users/Caspar/Desktop/test_mails/d.txt"

with open(file, "r") as f:
    data = f.read()

mail = '''For English, see below 
        Dear Caspar,  We hope you were able to visit the Bachelor’s Day. We enjoyed it immensely and we hope your visit 
        helped
you decide on a Bachelor’s programme.  How was your experience at the Bachelor’s Day? Let us know by filling out the
questionnaire. You might even win one of three bol.com/amazon gift cards worth €100 each.  
•	The questionnaire is easy to access on your mobile phone.
•	It will take just 5-10 minutes to complete.
•	Your answers are anonymous.

Your input will help us to keep improving the Bachelor’s Day experience for future students.

Start the questionnaire

Thank you for your input!

University of Amsterdam

PS. Don’t forget to leave your email address at the end of the questionnaire for a chance to win one of the
three bol.com/amazon gift cards worth €100 each.

Unsubscribe'''


def mail_analyse(data):

    data = data.lower()

    lijst_met_woorden = word_tokenize(data)

    currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
    for element1 in lijst_met_woorden:
        index_element1 = lijst_met_woorden.index(element1)
        if element1 in currencies:
            lijst_met_woorden[index_element1] = "geldteken"

    for element2 in lijst_met_woorden:
        index_element2 = lijst_met_woorden.index(element2)
        if element2.isdigit():
            lijst_met_woorden[index_element2] = "getal"

    leesteken = string.punctuation
    lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

    woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
    tekens = []
    for element4 in lijst_met_woorden:
        for ws_functie_element4 in woordsoort_functie:
            if ws_functie_element4 == (element4, 'POS') or ws_functie_element4 == (element4, ':')\
                    or ws_functie_element4 == (element4, '``'):
                tekens.append(element4)

    lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in tekens]

    for element5 in lijst_met_woorden:
        index_element5 = lijst_met_woorden.index(element5)
        for ws_functie_element5 in woordsoort_functie:
            if ws_functie_element5 == (element5, 'CD'):
                lijst_met_woorden[index_element5] = "getal"

    stopw = set(stopwords.words())
    lijst_met_woorden = [element6 for element6 in lijst_met_woorden if element6 not in stopw]

    lijst_met_woorden = {element7: True for element7 in lijst_met_woorden}

    return lijst_met_woorden

print(mail_analyse(data))
print("--- %s seconds ---" % (time.time() - start_time))
