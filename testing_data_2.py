# TEST READING ENRON DATA
from email.parser import Parser

file = "/Users/Caspar/Desktop/test_mails/d.txt"

with open(file, "r") as f:
    data = f.read()

print(data)

# Data met emails met afzender en ontvanger is hiervoor nodig
# MIME email structure = normale mail structure

email = Parser().parsestr(data)

print("Aan:", email['aan'])
print("Van:", email['van'])
print("Onderwerp:", email['onderwerp'])
print("Datum:", email['datum'])
# print("Body:", email.get_payload())
