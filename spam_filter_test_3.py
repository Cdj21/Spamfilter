# SPAMFILTER ENRON DATA MET NAIVE BAYES CLASSIFIER

# ALLE IMPORTS

import os
import string
import time

import nltk.classify.util
from nltk.corpus import stopwords
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
import random

start_time = time.time()

msg1 = '''Van: emilie van Dijk <faas2002@gmail.com>
Onderwerp: Doorst.: What does Linus Torvalds think of Python?
Datum: 15 oktober 2019 om 11:25:25 CEST
Aan: caspardejong2003@gmail.com



Begin doorgestuurd bericht:
Van: Quora Digest <digest-noreply@quora.com>
Onderwerp: What does Linus Torvalds think of Python?
Datum: 14 oktober 2019 om 18:14:54 CEST
Aan: faas2002@gmail.com

Answer: Just some of my own random guesses from reading his comments here and. . . 

Faas's Digest
TOP STORIES FOR YOU


What does Linus Torvalds think of Python?


Jim Xu, Down to the metal
Written Sep 29, 2012
Just some of my own random guesses from reading his comments here and there from Internet in the pass... Linus 
interests and cares about bare metal performance in an extr... Read More »

What would be the best way to make a your first million before age 24 and still have 2 days a week to socialize?


Kurt Guntheroth, Software Engineer and Writer
Updated Sep 14
Surefire recipe to make your first million by age 24 and still have 2 days a week to socialize.
	1.	Go to Saudi Arabia, Iraq, or Syria.
	2.	Wander in the desert for five days a week,...
 Read More »

Are Serbian and Croatian really different languages?


Goran Arsic, lives in Serbia (1976-present)
Updated Jul 13
Let me give you an example:If you were a native Serb and you would write a CV in Serbia and state that you speak 
Croatian (or Bosnian, Montenegrin) as a foreign language, e... Read More »

What is the most dangerous Python line of code?


Jack Schumann, B.S. from University of Virginia (2020)
Written Jan 5
You say Python in general, so I will allow myself to consider Python 2. By far the most ridiculous and absurd line
 of code I have seen is the following:True = False This is... Read More »
If 10^y=y^10, then what is y?

Nancy Mitchell, used to be a teacher.
Updated Sep 20
If 10^y=y^10, then what is y?The given equation is[math]10^y=y^{10}\text{.}[/math]Take the tenth root of each 
side and get... Read More »

How do narcissists describe their exes?


Gigi Anderson, former Finance
Written Aug 25, 2018
Well….this is what my ex narcissist said to me, after I broke up with him for the second time….”I will never say 
anything bad about you.” Now if someone has to say that to ... Read More »
What are the downsides of Python today?
Christian Daniel Griset, Data Science Engineering Manager at Stripe
Written Nov 15, 2018
Can you find the bug?class BankAccount(object): def __init__(self, amount = 0): self.amount = amount... Read More »
How do you say "bro" in Spanish?
Nelson Lopez, knows Spanish
Written Oct 1
It depends on the context. For example, if you are trying to say something like: “Bro, what's good! How are you? “ then 
it would be like:“Ostia tío, ¡que tal! ¿Cómo estáis?”... Read More »
Why is China upset with the NBA?
Nick Nian Ke, Ph.D Computer Science & Finance, University of Water (2020)
Written Oct 8
We still have the freedom to be upset right?What is the big deal? CCTV and Tencent terminate the contract. 
They are allowed to do so right?If you express your support to so... Read More »
Do people still use Ruby?
Thomas Brelop, former User at Business
Written Jan 23
Now it’s half past 2018! Ruby on Rails is now 14 years old. According to the latest usage information on BuiltWith 
Technology Lookup, Ruby on Rails is currently being used ... Read More »
Read More in Your Feed
Follow Quora on:

Get the App for iOS and Android

This email was sent by Quora (650 Castro Street #450, Mountain View, CA 94041).
If you don't want to receive this type of email in the future, please unsubscribe.
http://www.quora.com/


'''

msg2 = '''As one of our top customers we are providing 10% OFF the total of your next used book purchase from 
www.letthestoriesliveon.com. Please use the promotional code, TOPTENOFF at checkout. Limited to 1 use per customer.
 All books have free shipping within 
the contiguous 48 United States and there is no minimum purchase.

We have millions of used books in stock that are up to 90% off MRSP and add tens of thousands of new items every day. 
Don’t forget to check back frequently for new arrivals.'''

msg3 = '''To start off, I have a 6 new videos + transcripts in the members section. In it, we analyse the Enron email 
dataset, half a million files, spread over 2.5GB. It's about 1.5 hours of  video.

I have also created a Conda environment for running the code (both free and member lessons). This is to ensure everyone 
is running the same version of libraries, preventing the Works on my machine problems. If you get a second, do you mind
 trying it here?'''

rootdir = "/Users/Caspar/Desktop/Enron data"
# rootdir2 = "/Users/Caspar/Desktop/Enron data/enron1/ham"

ham_list = []

spam_list = []

for directory, subdirectory, filenames in os.walk(rootdir):
    if os.path.split(directory)[1] == 'ham':
        print(directory, subdirectory, len(filenames))
        for file in filenames:
            with open(os.path.join(directory, file), encoding="latin-1") as f:
                data = f.read()

                tekst = data.lower()
                lijst_met_woorden = word_tokenize(tekst)

                currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
                for c in lijst_met_woorden:
                    b = lijst_met_woorden.index(c)
                    if c in currencies:
                        lijst_met_woorden[b] = "geldteken"

                leesteken = string.punctuation
                lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

                woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
                # print(woordsoort_functie)

                stopw = set(stopwords.words())
                lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in stopw]

                for po in lijst_met_woorden:
                    a = lijst_met_woorden.index(po)
                    if po.isdigit():
                        lijst_met_woorden[a] = "getal"


                def naivebayes_woordenboek(lijst_met_woorden):
                    mijn_woordenboek = dict([(word, True) for word in lijst_met_woorden])
                    return mijn_woordenboek


                ham_list.append((naivebayes_woordenboek(lijst_met_woorden), "ham"))

    if os.path.split(directory)[1] == 'spam':
        print(directory, subdirectory, len(filenames))
        for file in filenames:
            with open(os.path.join(directory, file), encoding="latin-1") as f:
                data = f.read()

                tekst = data.lower()
                lijst_met_woorden = word_tokenize(tekst)

                currencies = {'$', '€', '£', '¥', 'usd', 'eur', 'gbp', 'yen'}
                for c in lijst_met_woorden:
                    b = lijst_met_woorden.index(c)
                    if c in currencies:
                        lijst_met_woorden[b] = "geldteken"

                leesteken = string.punctuation
                lijst_met_woorden = [element3 for element3 in lijst_met_woorden if element3 not in leesteken]

                woordsoort_functie = nltk.pos_tag(lijst_met_woorden)
                # print(woordsoort_functie)

                stopw = set(stopwords.words())
                lijst_met_woorden = [element5 for element5 in lijst_met_woorden if element5 not in stopw]

                for po in lijst_met_woorden:
                    a = lijst_met_woorden.index(po)
                    if po.isdigit():
                        lijst_met_woorden[a] = "getal"


                def naivebayes_woordenboek(lijst_met_woorden):
                    mijn_woordenboek = dict([(word, True) for word in lijst_met_woorden])
                    return mijn_woordenboek


                spam_list.append((naivebayes_woordenboek(lijst_met_woorden), "spam"))

combined_list = ham_list + spam_list

random.shuffle(combined_list)

training_part = int(len(combined_list) * .7)

training_set = combined_list[:training_part]
test_set = combined_list[training_part:]

classifier = NaiveBayesClassifier.train(training_set)
accuracy = nltk.classify.accuracy(classifier, test_set)

print("Eerste ham email" "\n")
print(ham_list[0])
print("Aantal emails in ham_list")
print(len(ham_list))

print("\n" "Eerste spam email" "\n")
print(spam_list[0])
print("Aantal emails in spam_list")
print(len(spam_list))

# print(naivebayes_woordenboek(lijst_met_woorden))

print(len(training_set))
print(len(test_set))

print("Accuracy = ", accuracy * 100)
classifier.show_most_informative_features(20)

words = word_tokenize(msg1)
features = naivebayes_woordenboek(words)
print("Message 1 is :", classifier.classify(features))

words = word_tokenize(msg2)
features = naivebayes_woordenboek(words)
print("Message 2 is :", classifier.classify(features))

words = word_tokenize(msg3)
features = naivebayes_woordenboek(words)
print("Message 3 is :", classifier.classify(features))

print("--- %s seconds ---" % (time.time() - start_time))
